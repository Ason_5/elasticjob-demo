package com.ason.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ImportResource(locations={"classpath:job.xml"})
public class BaseSimpleJob implements SimpleJob {
    @Override
    public void execute(ShardingContext shardingContext) {

    }
}